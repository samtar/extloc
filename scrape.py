import pymysql
import json
import requests
import sys
import toolforge
import time

toolforge.set_user_agent('extloc', 'https://extloc.toolforge.org')

api_path = '/w/api.php?action=query&meta=siteinfo&siprop=extensions&sifilteriw=local&format=json'

def get_urls():
    conn = toolforge.connect('meta', 'analytics', cursorclass=pymysql.cursors.DictCursor)

    urls = {}

    # conn is a pymysql.connection object.
    with conn.cursor() as cur:
        cur.execute('SELECT * FROM wiki')
        for row in cur:
            urls[ row['dbname'] ] = row['url']

    conn.close()

    return urls

def get_extensions(wiki_base_url):
    r = requests.get(f"{wiki_base_url}{api_path}")
    return r.json()['query']['extensions']

def main():
    wiki_to_extensions = {}
    extension_to_wikis = {}

    urls = get_urls()

    for wiki in urls:
        print(f"checking {wiki}")
        wiki_to_extensions[wiki] = []
        for ext in get_extensions(urls[wiki]):
            wiki_to_extensions[wiki].append(ext['name'])
            extension_to_wikis.setdefault(ext['name'], []).append(wiki)

        # This could live outside the loop, just re-writing on every wiki for
        # testing purposes.

        json_str = json.dumps(wiki_to_extensions, indent=4)
        with open('wiki-to-extensions.json', 'w') as version_file:
            version_file.write(json_str)

        json_str = json.dumps(extension_to_wikis, indent=4)
        with open('extension-to-wikis.json', 'w') as version_file:
            version_file.write(json_str)

        time.sleep(1)

main()
