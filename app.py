from flask import Flask, request, Response, render_template
import pymysql
import toolforge
import json

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/wikis')
def wikis():
    conn = toolforge.connect('meta', 'analytics', cursorclass=pymysql.cursors.DictCursor)

    result = ''

    # conn is a pymysql.connection object.
    with conn.cursor() as cur:
        cur.execute('SELECT * FROM wiki')
        for row in cur:
            # print(row['dbname'], row['url'])
            result = result + row['dbname'] + ' ' + row['url']

    conn.close()

    return result

@app.route('/extensions')
def extensions():
    with open('extension-to-wikis.json') as f:
        data = f.read()
    return Response(data, mimetype='application/json')

@app.route('/extensions/<string:ext_name>')
def show_extension(ext_name):
    with open('extension-to-wikis.json') as f:
        extensions_to_wikis = json.load(f)

    # Return the list of wikis for the extension, defaulting to an empty list
    # if we don't have any data:
    ext_list = extensions_to_wikis.get(ext_name, [])
    return Response(json.dumps(ext_list), mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True)
