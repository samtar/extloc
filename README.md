# extloc - find out where MediaWiki extensions are deployed in Wikimedia production

Original task: [T296050](https://phabricator.wikimedia.org/T296050).

## Code

Not much here, but I clearly messed with this for a bit.

  - `app.py` doesn't really do anything, was a stub I was going to hang useful
    endpoints on.
  - `scrape.py` will get a `versions.json` that gestures at something useful,
    tho it doesn't actually say anything about versions or groups at the moment.

## Background

The goal here was a thing to expose:

  - What wikis an extension is deployed to
  - What groups an extension is deployed within
  - What extensions are deployed to a wiki
  - What extensions are deployed to a group

This exists, and has a list of extensions: https://www.mediawiki.org/wiki/API:Siteinfo

You can see this list with something like:

```sh
curl 'https://www.mediawiki.org/w/api.php?action=query&meta=siteinfo&siprop=extensions&sifilteriw=local&format=json' -o siteinfo.json
jq '.query.extensions[].name' ./siteinfo.json
```

I didn't want to scrape each individual wiki, but deriving this from
`CommonSettings.php` and `InitialiseSettings.php` [would be tricky][config].

Once I resigned myself to scraping, bd808 [laid out a reasonable plan][bd808]:

  - Schedule a (weekly?) job to:
    - Loop over wikis from dblists or equivalent
    - Scrape API for extension list
    - Stash data
  - Present data to user

[config]: https://www.mediawiki.org/wiki/Manual:Configuration_settings
[bd808]: https://federation.p1k3.com/@bd808@octodon.social/110154276194169100

## Relevant docs

### Toolforge

  - Setting up a Python service: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web/Python
  - Scheduling a job: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Jobs_framework
  - Metadata database: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Metadata_database

### Dependencies

  - https://wikitech.wikimedia.org/wiki/User:Legoktm/toolforge_library
  - https://pymysql.readthedocs.io/
  - https://python-toolforge.readthedocs.io/

## Installation

https://toolsadmin.wikimedia.org/tools/id/extloc

## License

Licensed under the GPL-3.0-or-later license. See COPYING for the full license.
